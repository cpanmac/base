package com.common.bean.execption;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

public class BaseOauthExceptionSerializer extends StdSerializer<BaseOauthException> {

    public BaseOauthExceptionSerializer() {
        super(BaseOauthException.class);
    }

    @Override
    public void serialize(BaseOauthException value, JsonGenerator gen, SerializerProvider provider) throws IOException {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();

        value.getJsonGenerator(request.getServletPath(), gen, value);
    }
}