package com.common.cache;

import cn.hutool.cache.CacheUtil;
import cn.hutool.cache.impl.TimedCache;

/***
 * 本地超时缓存
 */
public class TimedLoaclCache {

    /**
     * 验证码缓存, 默认五分钟
     */
    public static final TimedCache<String, String> CODE_CACHE = CacheUtil.newTimedCache(60 * 5);

}
