package com.common.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * 数据Entity类
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class DataEntity<T> extends Model<DataEntity> {

    /**
     * 删除标记0：正常
     */
    public static final String DEL_FLAG_NORMAL = "0";
    /**
     * 删除标记1：删除
     */
    public static final String DEL_FLAG_DELETE = "1";

    /**
     * 主键
     */
    private Long id;
    /**
     * 创建日期
     */
    @TableField(fill = FieldFill.INSERT)
    private Date createDate;
    /**
     * 更新日期
     */
    @TableField(fill = FieldFill.UPDATE)
    private Date updateDate;
    /**
     * 创建者
     */
    @TableField(fill = FieldFill.INSERT)
    private Long createUserId;
    /**
     * 更新者
     */
    @TableField(fill = FieldFill.UPDATE)
    private Long updateUserId;
    /**
     * 备注
     */
    private String memo;

    /**
     * 删除标记(0:正常;1:删除;)
     */
    private String delFlag;

    /**
     * 搜索
     */
    private String searchKeys;
    /**
     * 日期开始
     */
    private String dateStart;
    /**
     * 结束日期
     */
    private String dateEnd;

    public DataEntity() {
        super();
        this.delFlag = DEL_FLAG_NORMAL;
    }

    public DataEntity(Long id) {
        this.id = id;
    }

    /**
     * 是否是新记录（默认：false），调用setIsNewRecord()设置新记录，使用自定义ID。
     * 设置为true后强制执行插入语句，ID不会自动生成，需从手动传入。
     *
     * @return 是否是新记录
     */
    @JsonIgnore
    public boolean getIsNewRecord() {
        return getId() == null || getId() == 0L;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
