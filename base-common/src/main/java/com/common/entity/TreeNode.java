package com.common.entity;

import cn.hutool.core.collection.CollUtil;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * 树形基类
 */
@Data
public class TreeNode extends DataEntity {

    /**
     * 父ID
     */
    protected Long parentId;

    /**
     * 子级
     */
    protected List<TreeNode> children;

    public void add(TreeNode node) {
        if(CollUtil.isEmpty(children)) {
            children = CollUtil.newArrayList();
        }
        children.add(node);
    }
}
