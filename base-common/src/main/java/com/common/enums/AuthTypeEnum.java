package com.common.enums;

import cn.hutool.core.util.StrUtil;

/**
 * 鉴权类型枚举
 */
public enum AuthTypeEnum {

    /**
     * web端
     */
    WEB("WEB", "web端"),

    /**
     * SMS 手机号
     */
    WEB_SMS("WEB_SMS", "SMS 手机号"),

    /**
     * web 社交账号
     */
    WEB_SOCIAL("WEB_SOCIAL", "SMS 手机号"),

    /**
     * APP端
     */
    APP("APP", "APP端"),

    /**
     * SMS 手机号
     */
    APP_SMS("APP_SMS", "SMS 手机号"),

    /**
     * APP 社交账号
     */
    APP_SOCIAL("APP_SOCIAL", "APP 社交账号");

    /**
     * 编码
     */
    private String code;

    /**
     * 描述
     */
    private String desc;

    AuthTypeEnum(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    /**
     * 根据编码获取
     * @param code
     * @return
     */
    public static AuthTypeEnum getByCode(String code) {
        for (AuthTypeEnum commonSEnum : AuthTypeEnum.values()) {
            if (StrUtil.equals(commonSEnum.getCode(), code)) {
                return commonSEnum;
            }
        }
        return null;
    }

    public String getSql() {
        return StrUtil.concat(true,"'", getCode(), "'");
    }

    public String getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }
}
