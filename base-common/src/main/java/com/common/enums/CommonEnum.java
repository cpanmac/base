package com.common.enums;

import cn.hutool.core.util.StrUtil;

/**
 * 常用常量
 */
public enum CommonEnum {

    /**
     * token请求头名称
     */
    REQ_HEADER("Authorization", "token请求头名称"),

    /**
     * token分割符
     */
    TOKEN_SPLIT("bearer ", "token分割符"),

    /**
     * jwt签名
     */
    SIGN_KEY("BASE", "jwt签名"),
    /**
     * 有效
     */
    VALID("VALID", "有效"),
    /**
     * 无效
     */
    INVALID("INVALID", "无效"),
    /**
     * 已删除
     */
    DEL_FLAG_DEL("DEL", "已删除"),
    /**
     * 正常
     */
    DEL_FLAG_NOR("NOR", "正常"),

    /**
     * 菜单
     */
    MENU("MENU", "菜单"),

    /**
     * 按钮
     */
    BUTTON("BUTTON", "按钮"),

    /**
     * admin ID
     */
    ADMIN_ID("1", "admin ID"),
    /**
     * 超级管理员角色ID
     */
    ADMIN_ROLE_ID("1", "超级管理员角色ID"),

    /**
     * 删除标记
     */
    DEL_FLAG("del_flag", "删除标记"),

    /**
     * 创建人标记
     */
    CREATE_USER_ID("create_user_id", "创建人标记"),

    /**
     * 更新人标记
     */
    UPDATE_USER_ID("update_user_id", "更新人标记"),

    /**
     * 创建时间标记
     */
    CREATE_DATE("create_date", "创建时间标记"),

    /**
     * 更新时间标记
     */
    UPDATE_DATE("update_date", "更新时间标记"),

    /**
     * 编码
     */
    UTF8("UTF-8", "编码"),

    /**
     * JSON 资源
     */
    CONTENT_TYPE("application/json; charset=utf-8", "JSON 资源"),

    /**
     * 树根节点
     */
    TREE_ROOT_NODE("-1", "树根节点");

    /**
     * 编码
     */
    private String code;

    /**
     * 描述
     */
    private String desc;

    CommonEnum(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    /**
     * 根据编码获取
     * @param code
     * @return
     */
    public static CommonEnum getByCode(String code) {
        for (CommonEnum commonSEnum : CommonEnum.values()) {
            if (StrUtil.equals(commonSEnum.getCode(), code)) {
                return commonSEnum;
            }
        }
        return null;
    }

    public String getSql() {
        return StrUtil.concat(true,"'", getCode(), "'");
    }

    public String getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }
}
