package com.common.mapper;

import cn.hutool.core.lang.Dict;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.common.entity.DataEntity;
import org.apache.ibatis.annotations.Param;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * DAO基类
 */
public interface BaseMapper<T extends DataEntity> {

    /**
     * <p>
     * 插入一条记录
     * </p>
     *
     * @param entity 实体对象
     */
    int insert(T entity);

    /**
     * <p>
     * 根据 ID 删除
     * </p>
     *
     * @param id 主键ID
     */
    int deleteById(Serializable id);

    /**
     * <p>
     * 根据 columnMap 条件，删除记录
     * </p>
     *
     * @param columnMap 表字段 map 对象
     */
    int deleteByMap(Map<String, Object> columnMap);

    /**
     * <p>
     * 根据 entity 条件，删除记录
     * </p>
     *
     * @param entity 实体对象封装操作类（可以为 null）
     */
    int delete(T entity);

    /**
     * <p>
     * 删除（根据ID 批量删除）
     * </p>
     *
     * @param idList 主键ID列表(不能为 null 以及 empty)
     */
    int deleteBatchIds(Collection<? extends Serializable> idList);

    /**
     * <p>
     * 根据 ID 修改
     * </p>
     *
     * @param entity 实体对象
     */
    int updateById(T entity);

    /**
     * <p>
     * 根据 whereEntity 条件，更新记录
     * </p>
     *
     * @param entity        实体对象 (set 条件值,不能为 null)
     */
    int update(T entity);

    /**
     * <p>
     * 根据 ID 查询
     * </p>
     *
     * @param id 主键ID
     */
    T getById(Serializable id);

    /**
     * <p>
     * 查询（根据ID 批量查询）
     * </p>
     *
     * @param idList 主键ID列表(不能为 null 以及 empty)
     */
    List<T> getBatchIds(Collection<? extends Serializable> idList);

    /**
     * <p>
     * 查询（根据 columnMap 条件）
     * </p>
     *
     * @param columnMap 表字段 map 对象
     */
    List<T> getByMap(Map<String, Object> columnMap);

    /**
     * <p>
     * 根据 entity 条件，查询一条记录
     * </p>
     *
     * @param entity 实体对象
     */
    T getOne(T entity);

    /**
     * <p>
     * 根据 entity 条件，查询总记录数
     * </p>
     *
     * @param entity 实体对象
     */
    Integer getCount(T entity);

    /**
     * <p>
     * 根据 entity 条件，查询全部记录
     * </p>
     *
     * @param entity 实体对象封装操作类（可以为 null）
     */
    List<T> getList(T entity);

    /**
     * <p>
     * 根据 entity 条件，查询全部记录
     * </p>
     *
     * @param entity 实体对象封装操作类（可以为 null）
     */
    List<Map<String, Object>> getMaps(T entity);

    /**
     * <p>
     * 根据 entity 条件，查询全部记录
     * 注意： 只返回第一个字段的值
     * </p>
     *
     * @param entity 实体对象封装操作类（可以为 null）
     */
    List<Object> getObjs(T entity);

    /**
     * <p>
     * 根据 entity 条件，查询全部记录（并翻页）
     * </p>
     *
     * @param page         分页查询条件（可以为 RowBounds.DEFAULT）
     * @param query 实体对象封装操作类（可以为 null）
     */
    IPage<T> getPage(IPage<T> page, @Param("query") T query);

    /**
     * <p>
     * 根据 entity 条件，查询全部记录（并翻页）
     * </p>
     *
     * @param page         分页查询条件（可以为 RowBounds.DEFAULT）
     * @param query 实体对象封装操作类（可以为 null） {@link cn.hutool.core.lang.Dict}
     */
    IPage<T> getPage(IPage<T> page, @Param("query") Dict query);

    /**
     * <p>
     * 根据 entity 条件，查询全部记录（并翻页）
     * </p>
     *
     * @param page         分页查询条件
     * @param query 实体对象封装操作类
     */
    IPage<Map<String, Object>> getMapsPage(IPage<T> page, @Param("query") T query);

    /**
     * <p>
     * 根据 entity 条件，查询全部记录（并翻页）
     * </p>
     *
     * @param page         分页查询条件
     * @param query 实体对象封装操作类  {@link cn.hutool.core.lang.Dict}
     */
    IPage<Map<String, Object>> getMapsPage(IPage<T> page, @Param("query") Dict query);


}