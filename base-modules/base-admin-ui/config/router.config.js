export default [
   {
      "path":"/upms",
      "routes":
      [
         {
            "redirect":"/upms/login",
            "path":"/upms"
         },
         {
            "path":"/upms/login",
            "component":"./System/Login"
         }

      ],
      "component":"../layouts/UserLayout"
   },
   {
      "path":"/code",
      "routes":
      [
         {
            "redirect":"/code/generate",
            "path":"/code"
         },
         {
            "path":"/code/generate",
            "component":"./System/Generate"
         }

      ],
      "component":"../layouts/BlankLayout"
   },
   {
      "path":"/",
      "routes":
      [
         {
            "path":"/system",
            "routes":
            [
               {
                  "path":"/system/dept",
                  "component":"./System/DeptList"
               },
               {
                  "path":"/system/role",
                  "component":"./System/RoleList"
               },
               {
                  "path":"/system/user",
                  "component":"./System/UserList"
               },
               {
                  "path":"/system/dict",
                  "component":"./System/DictList"
               },
               {
                  "path":"/system/menu",
                  "component":"./System/MenuList"
               },
               {
                  "path":"/system/log",
                  "component":"./System/LogList"
               }

            ]

         },
         {
            "path":"/exception",
            "routes":
            [
               {
                  "path":"/exception/403",
                  "component":"./Exception/403"
               },
               {
                  "path":"/exception/404",
                  "component":"./Exception/404"
               },
               {
                  "path":"/exception/500",
                  "component":"./Exception/500"
               }

            ]

         },

      ],
      "component":"../layouts/BasicLayout",
      "authority":
      [
         "admin",
         "user"
      ],
      "Routes":
      [
         "src/pages/Authorized"
      ]

   }

];