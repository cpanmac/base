import * as generate from '@/services/generate';
import {message} from 'antd';

export default {
  namespace: 'generate',
  state: {
    tableList: [],
    tableColumnList: [],
  },
  effects: {
    *getTableList({ payload }, { call, put }) {
      const { data, ok } = yield call(generate.getTableList, payload);
      if (ok) {
        yield put({
          type: 'save',
          payload: {
            tableList: data,
          },
        });
      }
    },
    *getTableColumnList({ payload, callback}, { call, put }) {
      const { data, ok } = yield call(generate.getTableColumnList, payload);
      if (ok) {
        yield put({
          type: 'save',
          payload: {
            tableColumnList: data,
          },
        });
        if (typeof callback === 'function') {
          callback();
        }
      }
    },
    *gen({ payload, callback }, { call, put }) {
      const { ok } = yield call(generate.gen, payload);
      if (ok) {
        if (typeof callback === 'function') {
          callback();
        }
        message.success('生成成功');
      } else {
        message.error('生成失败');
      }
    },
  },

  reducers: {
    save(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    },
  },
};
