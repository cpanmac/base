import * as system from '@/services/upms';
import {pagination} from '@/config'

export default {
  namespace: 'sysLog',
  state: {
    list: [],
    pagination,
  },

  effects: {
    *getList({ payload }, { call, put }) {
      const { data, ok } = yield call(system.getLogList, payload);
      if (ok) {
        yield put({
          type: 'save',
          payload: {
            list: data.records,
            pagination: {
              current: Number(data.current) || 1,
              pageSize: Number(data.size) || 20,
              total: data.total,
            }
          },
        });
      }
    },
  },

  reducers: {
    save(state, action) {
      const { list, pagination } = action.payload
      return {
        ...state,
        list: list,
        pagination: {...pagination}
      };
    },
  },
};
