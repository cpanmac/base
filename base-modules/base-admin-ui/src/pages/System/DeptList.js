import React, { Fragment, PureComponent } from 'react';
import { connect } from 'dva';
import {
  Badge,
  Button,
  Card,
  Col,
  Divider,
  Form,
  Input,
  InputNumber,
  message,
  Modal,
  Row,
  Select,
  Table,
  Popconfirm,
} from 'antd';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import Dict from '@/components/Dict';
import config from '@/config'

import styles from './System.less';

const FormItem = Form.Item;
const { Option } = Select;

const formItemLayout = {
  labelCol: {
    span: 8,
  },
  wrapperCol: {
    span: 14,
  },
};

@connect(({ sysDept, sysUser, loading }) => ({
  sysDept,
  sysUser,
  loading: loading.models.sysDept,
}))
@Form.create()
export default class DeptList extends PureComponent {
  state = {
    modalVisible: false,
    itemType: 'create',
    item: {},
    tempSorts: {},
    expandedRowKeys: ['1'],
    fileList: [],
  };

  componentDidMount() {
    this.initQuery();
  }

  initQuery() {
    const { dispatch } = this.props;
    dispatch({
      type: 'sysDept/getList',
    });
  }

  handleModalVisible = (flag, itemType, item) => {
    console.log(item)
    if (itemType === 'create') item = {};
    if (itemType === 'sub')
      item = {
        level: item.level,
        parentId: item.id,
        parentName: item.name,
      };
    if (!flag) item = {};
    this.setState({
      modalVisible: flag,
      itemType: itemType,
      item,
    });
  };

  handleAdd = fields => {
    this.props.dispatch({
      type: 'sysDept/add',
      payload: {
        ...fields,
      },
      callback: () => this.initQuery(),
    });
    this.setState({
      modalVisible: false,
    });
  };

  handleChangeSort(val, record) {
    let tempSorts = this.state.tempSorts;
    tempSorts[record.id.toString()] = { id: record.id, sort: val };
    if ((val || 0) === (record.sort || 0)) delete tempSorts[record.id];
    this.setState({ tempSorts: Object.assign({}, tempSorts) });
  }

  handleUpdateSorts() {
    let sorts = [];
    const { tempSorts } = this.state;
    Object.keys(tempSorts).map(key => {
      sorts.push(tempSorts[key]);
    });
    this.props.dispatch({
      type: 'sysDept/updateSorts',
      payload: sorts,
      callback: () => {
        this.initQuery();
        this.setState({ tempSorts: {} });
      },
    });
  }

  handleDeleteById(record) {
    this.props.dispatch({
      type: 'sysDept/deleteById',
      payload: record.id,
      callback: () => this.initQuery(),
    });
  }

  handleTableExpand = expandedRows => {
    this.setState({
      expandedRowKeys: expandedRows,
    });
  };

  render() {
    const { sysDept: { list }, sysUser: { currentUser }, loading } = this.props;
    const { modalVisible, itemType, item, tempSorts } = this.state;

    const columns = [
      {
        title: '部门名称',
        key: 'name',
        dataIndex: 'name',
      },
      // {
      //   title: '部门编码',
      //   key: 'code',
      //   dataIndex: 'code',
      // },
      // {
      //   title: '部门级别',
      //   key: 'levelDesc',
      //   dataIndex: 'levelDesc',
      // },
      {
        title: '部门类型',
        key: 'typeDesc',
        dataIndex: 'typeDesc',
      },
      {
        title: '排序',
        key: 'sort',
        dataIndex: 'sort',
        render: (val, record) => (
          <InputNumber
            disabled={record.id === '2'}
            min={0}
            defaultValue={val}
            onChange={v => this.handleChangeSort(v, record)}
          />
        ),
      },
      {
        title: '备注',
        key: 'memo',
        dataIndex: 'memo',
      },
      {
        title: '操作',
        width: 150,
        render: (val, record) => (
          <Fragment>
            <a onClick={() => this.handleModalVisible(true, 'update', record)}>
              修改<Divider type="vertical" />
            </a>
            {record.id !== 1 && (
                <Popconfirm
                  title="删除该部门（及其所有子部门）后将影响功能正常显示且无法找回，请谨慎使用！"
                  placement="topRight"
                  onConfirm={() => this.handleDeleteById(record)}
                >
                  <a>
                    删除<Divider type="vertical" />
                  </a>
                </Popconfirm>
              )}
            <a onClick={() => this.handleModalVisible(true, 'sub', record)}>新建子部门</a>
          </Fragment>
        ),
      },
    ];

    const createModalProps = {
      item,
      itemType,
      visible: modalVisible,
      dispatch: this.props.dispatch,
      handleAdd: this.handleAdd,
      handleModalVisible: () => this.handleModalVisible(false),
    };

    const CreateFormGen = () => <CreateForm {...createModalProps} />;

    return (
      <PageHeaderWrapper>
        <Card bordered={false}>
          <div className={styles.tableList}>
            <div className={styles.tableListOperator}>
              {Object.keys(tempSorts).length > 0 && (
                <Button type="primary" onClick={() => this.handleUpdateSorts()}>
                  更新排序
                </Button>
              )}
            </div>
            <Table
              expandedRowKeys={this.state.expandedRowKeys}
              loading={loading}
              dataSource={list}
              columns={columns}
              pagination={false}
              onExpandedRowsChange={this.handleTableExpand}
              rowKey={item => item.id}
            />
          </div>
        </Card>
        <CreateFormGen />
      </PageHeaderWrapper>
    );
  }
}

const CreateForm = Form.create()(props => {
  const { visible, form, itemType, item, handleAdd, handleModalVisible } = props;

  const okHandle = () => {
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      form.resetFields();
      handleAdd({
        ...fieldsValue,
        id: item.id,
        parentId: item.parentId || -1,
        status: item.status || config.STATUS_VALID,
      });
    });
  };

  const modalProps = {
    title: '新建',
    visible,
    onOk: okHandle,
    onCancel: handleModalVisible,
  };

  if (itemType === 'create') modalProps.title = '新建';
  if (itemType === 'update') modalProps.title = '修改';
  if (itemType === 'sub') modalProps.title = '新建子部门';
  return (
    <Modal {...modalProps}>
      <Form>
        <Row>
          <Col span={24}>
            <FormItem label="父部门：" hasFeedback {...formItemLayout}>
              {<span>{item.parentName || '无'}</span>}
            </FormItem>
          </Col>
          <Col span={24}>
            <FormItem label="部门名称：" hasFeedback {...formItemLayout}>
              {form.getFieldDecorator('name', {
                initialValue: item.name,
                rules: [
                  {
                    required: true,
                    message: '请输入部门名称',
                  },
                ],
              })(<Input />)}
            </FormItem>
          </Col>
          <Col span={24}>
            <FormItem label="部门类型：" hasFeedback {...formItemLayout}>
              {form.getFieldDecorator('type', {
                initialValue: form.getFieldValue('level') === 'EDUCATION' ? 'OTHER_TYPE' : item.type,
                rules: [
                  {
                    required: true,
                    message: '请选择部门类型',
                  },
                ],
              })(<Dict type={'TYPE'} disabled={form.getFieldValue('level') === 'EDUCATION'} />)}
            </FormItem>
          </Col>
          <Col span={24}>
            <FormItem label="部门排序：" hasFeedback {...formItemLayout}>
              {form.getFieldDecorator('sort', {
                initialValue: item.sort,
                rules: [
                  {
                    required: true,
                    message: '请输入部门顺序',
                  },
                ],
              })(<InputNumber min={0} style={{ width: '100%' }} />)}
            </FormItem>
          </Col>
          <Col span={24}>
            <FormItem label="备注：" hasFeedback {...formItemLayout}>
              {form.getFieldDecorator('memo', {
                initialValue: item.memo,
              })(<Input.TextArea rows={2} />)}
            </FormItem>
          </Col>
        </Row>
      </Form>
    </Modal>
  );
});
