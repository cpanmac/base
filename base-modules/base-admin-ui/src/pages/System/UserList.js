import React, {Fragment, PureComponent} from 'react';
import {connect} from 'dva';
import {
  Badge,
  Button,
  Card,
  Col,
  Divider,
  Form,
  Input,
  InputNumber,
  Radio,
  message,
  Modal,
  Row,
  Select,
  Table,
  TreeSelect,
  Tree,
  Popconfirm,
} from 'antd';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import styles from './System.less';
import Dict from '@/components/Dict';
import ListTree from '@/components/ListTree';
import * as system from '@/services/upms';
import config from '@/config';

const FormItem = Form.Item;
const TreeNode = Tree.TreeNode;

const statusMap = {VALID: 'success', INVALID: 'error'};

const formItemLayout = {
  labelCol: {
    span: 8,
  },
  wrapperCol: {
    span: 14,
  },
};

@connect(({sysUser, sysDept, sysRole, loading}) => ({
  sysUser,
  sysDept,
  sysRole,
  loading: loading.models.sysUser,
}))
@Form.create()
export default class UserList extends PureComponent {
  state = {
    modalVisible: false,
    passwordModalVisible: false,
    assignModalVisible: false,
    itemType: 'create',
    item: {},
    deptId: '',
    dept: {},
  };

  componentDidMount() {
    this.initQuery();
  }

  initQuery = () => {
    const {dispatch} = this.props;
    dispatch({
      type: 'sysDept/getList',
    });
    this.initQueryUser();
    dispatch({
      type: 'sysRole/getList',
    });
  };

  initQueryUser = selectKeys => {
    let deptId = this.state.deptId;
    if (selectKeys && selectKeys.length > 0) {
      deptId = selectKeys && selectKeys.length > 0 ? (selectKeys[0] !== '1' ? selectKeys[0] : '') : '';
      this.setState({
        deptId: deptId,
      });
    }
    const {dispatch, form} = this.props;
    dispatch({
      type: 'sysUser/getList',
      payload: {
        deptId: deptId,
        ...form.getFieldsValue(),
      },
    });
  };

  handleFormReset = () => {
    this.props.form.resetFields();
  };

  handleModalVisible = (flag, itemType, item) => {
    if (itemType === 'create') item = {deptId: this.state.deptId};
    if (!flag) item = {};
    this.setState({
      modalVisible: flag,
      itemType: itemType,
      item,
    });
  };

  handlePasswordModalVisible = (flag, item) => {
    if (!flag) item = {};
    this.setState({
      passwordModalVisible: flag,
      item,
    });
  };

  handleAdd = fields => {
    this.props.dispatch({
      type: 'sysUser/add',
      payload: {
        ...fields,
      },
      callback: () => this.initQuery(),
    });
    this.setState({
      modalVisible: false,
    });
  };

  handleResetPassword = record => {
    this.props.dispatch({
      type: 'sysUser/resetPassword',
      payload: {
        id: record.id,
        type: 'INIT',
      },
    });
  };

  handleDelete(record) {
    this.props.dispatch({
      type: 'sysUser/delete',
      payload: record.id,
      callback: () => this.initQuery(),
    });
  }

  handleChangeState(record) {
    this.props.dispatch({
      type: 'sysUser/updateStatus',
      payload: {
        id: record.id,
        status: record.status === 'VALID' ? 'INVALID' : 'VALID',
      },
      callback: () => this.initQuery(),
    });
  }

  handlePageChange = page => {
    this.props.dispatch({
      type: 'sysUser/getList',
      payload: {
        current: page.current,
        size: page.pageSize,
        deptId: this.state.deptId,
        ...this.props.form.getFieldsValue(),
      },
    });
  };

  getSelectTreeData = list => {
    return list.map(item => {
      if (item.children && item.children.length > 0) {
        return {
          label: item.name,
          value: item.id,
          key: item.id,
          children: this.getSelectTreeData(item.children),
        };
      }
      return {
        label: item.name,
        value: item.id,
        key: item.id,
      };
    });
  };

  render() {
    const {sysUser: {list = [], pagination = {}, currentUser}, loading, form} = this.props;
    const {modalVisible, passwordModalVisible, itemType, item} = this.state;

    const columns = [
      {
        title: '用户名称',
        key: 'userName',
        dataIndex: 'userName',
      },
      {
        title: '所属部门',
        key: 'deptName',
        dataIndex: 'deptName',
      },
      {
        title: '登录账号',
        key: 'loginName',
        dataIndex: 'loginName',
      },
      {
        title: '用户类型',
        key: 'typeDesc',
        dataIndex: 'typeDesc',
      },
      {
        title: '电子邮箱',
        key: 'email',
        dataIndex: 'email',
      },
      {
        title: '手机',
        key: 'mobile',
        dataIndex: 'mobile',
      },
      {
        title: '状态',
        key: 'statusDesc',
        dataIndex: 'statusDesc',
        render: (val, record) => <Badge status={statusMap[record.status]} text={val}/>,
      },
      {
        title: '备注',
        key: 'memo',
        dataIndex: 'memo',
      },
      {
        title: '操作',
        width: 150,
        render: (val, record) => (
          <Fragment>
            <a onClick={() => this.handleModalVisible(true, 'update', record)}>
              修改<Divider type="vertical"/>
            </a>
            {record.status === config.STATUS_VALID ? (
              <Popconfirm
                title="禁用该用户后该用户将无法登录系统，请谨慎使用！"
                placement="topRight"
                onConfirm={() => this.handleChangeState(record)}
              >
                <a>
                  禁用<Divider type="vertical"/>
                </a>
              </Popconfirm>
            ) : (
              <Popconfirm
                title="启用该用户后该用户将正常登录系统，请谨慎使用！"
                placement="topRight"
                onConfirm={() => this.handleChangeState(record)}
              >
                <a>
                  启用<Divider type="vertical"/>
                </a>
              </Popconfirm>
            )}
            {record.type === 'SYSTEM' && (
              <Popconfirm
                title="删除该用户后将该用户将无法登录系统，请谨慎使用！"
                placement="topRight"
                onConfirm={() => this.handleDelete(record)}
              >
                <a>
                  删除<Divider type="vertical"/>
                </a>
              </Popconfirm>
            )}

            <Popconfirm
              title="重置密码后将会自动把密码重置为默认密码，请谨慎使用！"
              placement="topRight"
              onConfirm={() => this.handleResetPassword(record)}
            >
              <a>重置密码</a>
            </Popconfirm>
          </Fragment>
        ),
      },
    ];

    const createModalProps = {
      item,
      itemType,
      currentUser,
      selectTreeData: this.getSelectTreeData(this.props.sysDept.list),
      roleList: this.props.sysRole.list,
      visible: modalVisible,
      dispatch: this.props.dispatch,
      handleAdd: this.handleAdd,
      handleModalVisible: () => this.handleModalVisible(false),
    };

    const renderTreeNodes = data => {
      return data.map(item => {
        if (item.children && item.children.length > 0) {
          return (
            <TreeNode title={item.name} key={item.id} dataRef={item}>
              {renderTreeNodes(item.children)}
            </TreeNode>
          );
        }
        return <TreeNode title={item.name} key={item.id}/>;
      });
    };

    const CreateFormGen = () => <CreateForm {...createModalProps} />;

    return (
      <PageHeaderWrapper>
        <Row gutter={16}>
          <Col span={6}>
            <Card bordered={false}>
              <Tree defaultExpandedKeys={['1']} onSelect={this.initQueryUser}>
                {renderTreeNodes(this.props.sysDept.list)}
              </Tree>
            </Card>
          </Col>
          <Col span={18}>
            <Card bordered={false}>
              <div className={styles.tableList}>
                <div className={styles.tableListForm}>
                  <Form onSubmit={this.handleSearch} layout="inline">
                    <Row gutter={{md: 8, lg: 24, xl: 48}}>
                      <Col md={6} sm={24}>
                        <FormItem>
                          {form.getFieldDecorator('searchKeys')(
                            <Input placeholder="请输入需查询的用户名称、登录账号"/>
                          )}
                        </FormItem>
                      </Col>
                      <Col md={6} sm={24}>
                        <FormItem>
                          {form.getFieldDecorator('status', {
                            initialValue: '',
                          })(<Dict type={'STATUS'} radio query/>)}
                        </FormItem>
                      </Col>
                      <Col md={6} sm={24}>
                        <FormItem>
                          {form.getFieldDecorator('type', {
                            initialValue: '',
                          })(<Dict type={'TYPE'} radio query/>)}
                        </FormItem>
                      </Col>
                      <Col md={6} sm={24}>
                        <span className={styles.submitButtons}>
                          <Button type="primary" onClick={() => this.initQuery()}>
                            查询
                          </Button>
                          <Button style={{marginLeft: 8}} onClick={this.handleFormReset}>
                            重置
                          </Button>
                        </span>
                      </Col>
                    </Row>
                  </Form>
                </div>
                <div className={styles.tableListOperator}>
                  <Button type="primary" onClick={() => this.handleModalVisible(true, 'create')}>
                    新建
                  </Button>
                </div>
                <Table
                  loading={loading}
                  dataSource={list}
                  columns={columns}
                  pagination={pagination}
                  onChange={this.handlePageChange}
                  rowKey={item => item.id}
                />
              </div>
            </Card>
          </Col>
        </Row>
        <CreateFormGen/>
      </PageHeaderWrapper>
    );
  }
}

const CreateForm = Form.create()(props => {
  const {
    visible,
    form,
    itemType,
    item,
    roleList,
    currentUser,
    selectTreeData,
    handleAdd,
    handleModalVisible,
  } = props;

  const okHandle = () => {
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      form.resetFields();
      let formValues = {
        ...fieldsValue,
        id: item.id,
        status: item.status || config.STATUS_VALID,
        type: item.type || 'SYSTEM',
      };
      delete formValues.resPassword;
      if (itemType === 'update') delete formValues.password;
      if (formValues.rolesTemp && formValues.rolesTemp.length > 0) {
        formValues.roleList = formValues.rolesTemp.map(item => {
          return {id: item};
        });
        delete formValues.rolesTemp;
      }
      handleAdd(formValues);
    });
  };

  const modalProps = {
    title: '新建',
    visible,
    onOk: okHandle,
    onCancel: handleModalVisible,
  };

  if (itemType === 'create') modalProps.title = '新建';
  if (itemType === 'update') {
    modalProps.title = '修改';
  }

  const defaultRoles = item.roleIds ? item.roleIds.split(',') : undefined;

  return (
    <Modal {...modalProps}>
      <Form>
        <Row>
          <Col span={12}>
            <FormItem label="用户名称：" hasFeedback {...formItemLayout}>
              {form.getFieldDecorator('userName', {
                initialValue: item.userName,
                rules: [
                  {
                    required: true,
                    message: '请输入用户名称',
                  },
                ],
              })(<Input/>)}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="所属部门：" hasFeedback {...formItemLayout}>
              {form.getFieldDecorator('deptId', {
                initialValue: item.deptId,
                rules: [
                  {
                    required: true,
                    message: '请选择所属部门',
                  },
                ],
              })(
                <TreeSelect
                  style={{width: '100%'}}
                  dropdownStyle={{maxHeight: 400, overflow: 'auto'}}
                  treeData={selectTreeData}
                  treeDefaultExpandAll
                />
              )}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={12}>
            <FormItem label="登录账号：" hasFeedback {...formItemLayout}>
              {form.getFieldDecorator('loginName', {
                initialValue: item.loginName,
                rules: [
                  {
                    required: true,
                    message: '请输入登录账号',
                  },
                  {
                    async validator(rule, value, callback) {
                      if (!value) return;
                      const data = await system.isUserExists({
                        loginName: value,
                        id: item.id,
                      });
                      if (data.data) callback('该登录账号已存在');
                      callback();
                    },
                  },
                ],
              })(<Input disabled={item.id === '1'}/>)}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="所属角色：" hasFeedback {...formItemLayout}>
              {form.getFieldDecorator('rolesTemp', {
                initialValue: defaultRoles,
                rules: [
                  {
                    required: true,
                    message: '请选择角色',
                  },
                ],
              })(
                <Select style={{width: '100%'}} mode={'multiple'}>
                  {roleList &&
                  roleList.map(item => (
                    <Select.Option key={item.id} vlaue={item.id}>
                      {item.name}
                    </Select.Option>
                  ))}
                </Select>
              )}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={12}>
            <FormItem label="电子邮件：" hasFeedback {...formItemLayout}>
              {form.getFieldDecorator('email', {
                initialValue: item.email,
                rules: [
                  {
                    type: 'email',
                    message: '请输入正确的电子邮件地址',
                  },
                ],
              })(<Input/>)}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="手机号码：" hasFeedback {...formItemLayout}>
              {form.getFieldDecorator('mobile', {
                initialValue: item.mobile,
                rules: [
                  {
                    message: '请输入手机号码',
                  },
                  {
                    validator: (rule, value, callback) => {
                      if (value && !/^1[123456789]\d{9}$/.test(value)) {
                        callback('输入的手机号码有误');
                      }
                      callback();
                    },
                  },
                ],
              })(<Input/>)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <FormItem label="备注：" hasFeedback labelCol={{span: 4}} wrapperCol={{span: 19}}>
              {form.getFieldDecorator('memo', {
                initialValue: item.memo,
              })(<Input.TextArea rows={2}/>)}
            </FormItem>
          </Col>
        </Row>
      </Form>
    </Modal>
  );
});
