import {stringify} from 'qs';
import request from '@/utils/request';

export async function getTableList(params) {
  return request('/code/generate/getTableList');
}

export async function getTableColumnList(params) {
  return request(`/code/generate/getTableColumnList?${stringify(params)}`);
}

export async function gen(params) {
  return request(`/code/generate/generate`, {
    method: 'POST',
    body: params,
  });
}