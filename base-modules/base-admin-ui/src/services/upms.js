import {stringify} from 'qs';
import request from '@/utils/request';

export async function userLogin(params) {
  return request('/oauth/token', {
    method: 'POST',
    body: params,
  });
}

export async function userLogout(params) {
  return request('/auth/logout');
}

export async function userMenu() {
  return request('/sys/menu/userMenu');
}

export async function getCurrentUser() {
  return request('/sys/user/current');
}

export async function getDeptList(params) {
  return request(`/sys/dept/list?${stringify(params)}`);
}

export async function isDeptCodeExists(params) {
  return request(`/sys/dept/isCodeExists?${stringify(params)}`);
}

export async function saveDept(params) {
  return request(`/sys/dept/save`, {
    method: 'POST',
    body: params,
  });
}

export async function updateDeptSorts(params) {
  return request(`/sys/dept/updateSorts`, {
    method: 'POST',
    body: params,
  });
}

export async function deleteDept(id) {
  return request(`/sys/dept/${id}`, {
    method: 'DELETE',
  });
}

export async function getDictList(params) {
  return request(`/sys/dict/listPage?${stringify(params)}`);
}

export async function getDictlistType(params) {
  return request(`/sys/dict/listType?${stringify(params)}`);
}

export async function getListByParentCode(params) {
  return request(`/sys/dict/getListByParentCode?${stringify(params)}`);
}

export async function isDictCodeExists(params) {
  return request(`/sys/dict/isCodeExists?${stringify(params)}`);
}

export async function saveDict(params) {
  return request(`/sys/dict/save`, {
    method: 'POST',
    body: params,
  });
}

export async function updateDictSorts(params) {
  return request(`/sys/dict/updateSorts`, {
    method: 'POST',
    body: params,
  });
}

export async function updateDictStates(params) {
  return request(`/sys/dict/updateStatus`, {
    method: 'POST',
    body: params,
  });
}

export async function deleteDict(params) {
  return request(`/sys/dict/${stringify(params)}`, {
    method: 'DELETE',
  });
}

export async function getMenuList(params) {
  return request(`/sys/menu/listTree?${stringify(params)}`);
}

export async function saveMenu(params) {
  return request(`/sys/menu/save`, {
    method: 'POST',
    body: params,
  });
}

export async function updateMenuSorts(params) {
  return request(`/sys/menu/updateSorts`, {
    method: 'POST',
    body: params,
  });
}

export async function updateMenuStates(params) {
  return request(`/sys/menu/updateStatus`, {
    method: 'POST',
    body: params,
  });
}

export async function deleteMenu(params) {
  return request(`/sys/menu/${stringify(params)}`, {
    method: 'DELETE',
  });
}

export async function getRoleList(params) {
  return request(`/sys/role/list`);
}

export async function isRoleCodeExists(params) {
  return request(`/sys/role/isCodeExists?${stringify(params)}`);
}

export async function saveRole(params) {
  return request(`/sys/role/save`, {
    method: 'POST',
    body: params,
  });
}

export async function updateRoleStates(params) {
  return request(`/sys/role/updateStatus`, {
    method: 'POST',
    body: params,
  });
}

export async function saveMenuAuth(params) {
  return request(`/sys/role/saveMenuAuth`, {
    method: 'POST',
    body: params,
  });
}

export async function assignUser(params) {
  return request(`/sys/role/assignUser`, {
    method: 'POST',
    body: params,
  });
}

export async function deleteRoleUser(params) {
  return request(`/sys/role/deleteRoleUser`, {
    method: 'POST',
    body: params,
  });
}

export async function deleteRole(id) {
  return request(`/sys/role/${id}`, {
    method: 'DELETE',
  });
}

export async function getUserList(params) {
  return request(`/sys/user/listPage?${stringify(params)}`);
}

export async function getUsersByRoleId(params) {
  return request(`/sys/user/getUsersByRoleId?${stringify(params)}`);
}

export async function isUserExists(params) {
  return request(`/sys/user/isExists?${stringify(params)}`);
}

export async function saveUser(params) {
  return request(`/sys/user/save`, {
    method: 'POST',
    body: params,
  });
}

export async function resetPassword(params) {
  return request(`/sys/user/resetPassword`, {
    method: 'POST',
    body: params,
  });
}

export async function updateUserStates(params) {
  return request(`/sys/user/updateStatus`, {
    method: 'POST',
    body: params,
  });
}

export async function deleteUser(id) {
  return request(`/sys/user/${id}`, {
    method: 'DELETE',
  });
}

export async function getLogList(params) {
  return request(`/sys/log/listPage?${stringify(params)}`);
}
