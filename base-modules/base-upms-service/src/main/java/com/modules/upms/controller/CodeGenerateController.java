package com.modules.upms.controller;

import cn.hutool.core.lang.Dict;
import com.common.util.R;
import com.modules.upms.service.CodeGenerateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 代码生成 前端控制器
 * </p>
 */
@RestController
@RequestMapping(value = "/code/generate")
public class CodeGenerateController {

    @Autowired
    private CodeGenerateService codeGenerateService;

    /**
     * 获取所有的表结构
     *
     * @return 表结构
     */
    @GetMapping(value = "/getTableList")
    public R<List<Map<String, Object>>> getTableList() {
        return R.success(codeGenerateService.getTableList());
    }

    /**
     * 获取所有的字段结构
     *
     * @return 字段结构
     */
    @GetMapping(value = "/getTableColumnList")
    public R<List<Map<String, Object>>> getTableColumnList(String tableName) {
        return R.success(codeGenerateService.getTableColumnList(tableName));
    }

    /**
     * 生成
     *
     * @param param
     * @return success/false
     */
    @PostMapping(value = "/generate")
    public R<Boolean> generate(@RequestBody Dict param) {
        codeGenerateService.generate(param);
        return R.success();
    }
}
