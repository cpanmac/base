package com.modules.upms.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.common.util.R;
import com.common.web.BaseController;
import com.modules.upms.entity.SysLog;
import com.modules.upms.service.SysLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 日志表 前端控制器
 * </p>
 *
 * @author lengleng
 * @since 2017-11-20
 */
@RestController
@RequestMapping(value = "/sys/log")
public class SysLogController extends BaseController {
    @Autowired
    private SysLogService sysLogService;

    /**
     * 分页查询日志信息
     *
     * @param query 分页对象
     * @return 分页对象
     */
    @GetMapping(value = "/listPage")
    public R<IPage> listPage(Page page, SysLog query) {
        return R.success(sysLogService.getPage(page, query));
    }

}
