package com.modules.upms.entity;

import com.common.entity.TreeNode;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 菜单权限表
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class SysMenu extends TreeNode {

    private static final long serialVersionUID = 1L;

    /**
     * 菜单名称
     */
	private String name;

    /**
     * 多语言KEY
     */
	private String localesKey;
    /**
     * 前端URL
     */
    private String path;
    /**
     * 请求链接
     */
	private String url;
    /**
     * 菜单权限标识
     */
    private String permission;
    /**
     * 图标
     */
	private String icon;
    /**
     * 排序值
     */
	private Integer sort;
    /**
     * 菜单类型
     */
	private String type;
    /**
     * 菜单类型描述
     */
	private String typeDesc;
    /**
     * 状态
     */
	private String status;
    /**
     * 状态描述
     */
	private String statusDesc;

}
