package com.modules.upms.entity;

import cn.hutool.core.bean.BeanUtil;
import com.common.entity.DataEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 客户端
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class SysOauthClientDetails extends DataEntity {

    private static final long serialVersionUID = 1L;

    private String clientId;
    private String resourceIds;
    private String clientSecret;
    private String scope;
    private String authorizedGrantTypes;
    private String webServerRedirectUri;
    private String authorities;
    private Integer accessTokenValidity;
    private Integer refreshTokenValidity;
    private String additionalInformation;
    private String autoapprove;

    @Override
    protected Serializable pkVal() {
        return this.clientId;
    }

    @Override
    public String toString() {
        BeanUtil.beanToMap(this);
        return "SysOauthClientDetails{" +
                ", clientId=" + clientId +
                ", resourceIds=" + resourceIds +
                ", clientSecret=" + clientSecret +
                ", scope=" + scope +
                ", authorizedGrantTypes=" + authorizedGrantTypes +
                ", webServerRedirectUri=" + webServerRedirectUri +
                ", authorities=" + authorities +
                ", accessTokenValidity=" + accessTokenValidity +
                ", refreshTokenValidity=" + refreshTokenValidity +
                ", additionalInformation=" + additionalInformation +
                ", autoapprove=" + autoapprove +
                "}";
    }
}
