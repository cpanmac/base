package com.modules.upms.entity;

import com.common.entity.DataEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 角色菜单表
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class SysRoleMenu extends DataEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 角色ID
     */
	private Integer roleId;
    /**
     * 菜单ID
     */
	private Integer menuId;
}
