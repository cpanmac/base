package com.modules.upms.entity;

import com.common.entity.DataEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 用户角色表
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class SysUserRole extends DataEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 用户ID
     */
	private Integer userId;
    /**
     * 角色ID
     */
	private Integer roleId;

}
