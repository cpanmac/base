package com.modules.upms.entity.resp;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 验证码返回类
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class CodeResp implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 验证码
     */
    @ApiModelProperty(notes = "验证码")
    private String code;
    /**
     * 有效时间
     */
    @ApiModelProperty(notes = "有效时间（秒）")
    private Long expiration;

}
