package com.modules.upms.entity.vo;

import cn.hutool.core.util.StrUtil;
import com.common.constant.SecurityConstants;
import com.modules.upms.entity.SysRole;
import com.modules.upms.entity.SysUser;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 */
public class AuthUserVO implements UserDetails {
    private static final long serialVersionUID = 1L;
    private Long userId;
    private String userName;
    private String userType;
    private String password;
    private String status;
    private List<SysRole> roleList;

    public AuthUserVO(SysUser userVo) {
        this.userId = userVo.getId();
        this.userName = userVo.getUserName();
        this.userType = userVo.getType();
        this.password = userVo.getPassword();
        this.status = userVo.getStatus();
        roleList = userVo.getRoleList();
    }

    public AuthUserVO(Long userId, String userName, String userType, String status) {
        this.userId = userId;
        this.userType = userType;
        this.userName = userName;
        this.status = status;
    }


    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<GrantedAuthority> authorityList = new ArrayList<>();
        for (SysRole role : roleList) {
            authorityList.add(new SimpleGrantedAuthority(role.getCode()));
        }
        authorityList.add(new SimpleGrantedAuthority(SecurityConstants.BASE_ROLE));
        return authorityList;
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public String getUsername() {
        return this.userName;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return !StrUtil.equals("LOCK", status);
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return StrUtil.equals("VALID", status);
    }

    public void setUsername(String userName) {
        this.userName = userName;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<SysRole> getRoleList() {
        return roleList;
    }

    public void setRoleList(List<SysRole> roleList) {
        this.roleList = roleList;
    }

    public String getStatus() {
        return status;
    }

    public Long getUserId() {
        return userId;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
