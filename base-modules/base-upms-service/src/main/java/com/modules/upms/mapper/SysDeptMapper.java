package com.modules.upms.mapper;

import com.common.mapper.BaseMapper;
import com.modules.upms.entity.SysDept;

/**
 * <p>
 * 部门管理 Mapper 接口
 * </p>
 */
public interface SysDeptMapper extends BaseMapper<SysDept> {

    /**
     * 更新排序
     * @param sysDept
     */
    void updateSorts(SysDept sysDept);
}