package com.modules.upms.mapper;

import com.common.mapper.BaseMapper;
import com.modules.upms.entity.SysDict;

import java.util.List;

/**
 * <p>
  * 字典表 Mapper 接口
 * </p>
 */
public interface SysDictMapper extends BaseMapper<SysDict> {

    /**
     * 获取指定编码下的所有子字典
     * @param type
     * @return
     */
    List<SysDict> getListByType(String type);

    /**
     * 更新字典排序
     * @param sysDict
     */
    void updateSort(SysDict sysDict);

    /**
     * 获取所有的类型
     * @return
     */
    List<SysDict> getListType();

    /**
     * 更新状态
     * @param sysDict
     */
    void updateStatus(SysDict sysDict);
}