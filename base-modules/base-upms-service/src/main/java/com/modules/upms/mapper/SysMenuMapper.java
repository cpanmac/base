package com.modules.upms.mapper;

import com.common.mapper.BaseMapper;
import com.modules.upms.entity.SysMenu;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 菜单权限表 Mapper 接口
 * </p>
 */
public interface SysMenuMapper extends BaseMapper<SysMenu> {

    /**
     * 通过角色名查询菜单
     *
     * @param role 角色名称
     * @return 菜单列表
     */
    List<SysMenu> findMenuByRoleName(String role);

    /**
     * 根据用户ID获取菜单
     * @param userId
     * @return
     */
    List<SysMenu> findMenuByUserId(@Param("userId") Long userId);

    /**
     * 更新排序
     * @param sysMenu
     */
    void updateSort(SysMenu sysMenu);

    /**
     * 更新状态
     * @param sysMenu
     */
    void updateStatus(SysMenu sysMenu);

    /**
     * 根据前端URL查询菜单
     * @param path
     * @return
     */
    SysMenu getByPath(String path);

    /**
     * 获取最大的排序
     * @param path
     * @return
     */
    int getMaxSort(String path);
}