package com.modules.upms.mapper;


import com.common.mapper.BaseMapper;
import com.modules.upms.entity.SysOauthClientDetails;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lengleng
 * @since 2018-05-15
 */
public interface SysOauthClientDetailsMapper extends BaseMapper<SysOauthClientDetails> {

}
