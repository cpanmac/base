package com.modules.upms.service;

import cn.hutool.core.lang.Dict;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 代码生成 服务类
 * </p>
 */
public interface CodeGenerateService {

    /**
     * 获取所有的表结构
     */
    List<Map<String, Object>> getTableList();

    /**
     * 获取所有的字段结构
     */
    List<Map<String, Object>> getTableColumnList(String tableName);

    /**
     * 生成
     */
   void generate(Dict param);
}
