package com.modules.upms.service;

import com.modules.upms.entity.SysAttachmentInfo;
import com.common.service.BaseService;

/**
 * <p>
 * 系统附件表 服务类
 * </p>
 *
 * @author xmh
 */
public interface SysAttachmentInfoService extends BaseService<SysAttachmentInfo> {

}
