package com.modules.upms.service;

import com.common.service.BaseService;
import com.modules.upms.entity.SysLog;

/**
 * <p>
 * 日志表 服务类
 * </p>
 *
 * @author lengleng
 * @since 2017-11-20
 */
public interface SysLogService extends BaseService<SysLog> {

}
