package com.modules.upms.service;


import com.common.service.BaseService;
import com.modules.upms.entity.SysMenu;

import java.util.List;

/**
 * <p>
 * 菜单权限表 服务类
 * </p>
 */
public interface SysMenuService extends BaseService<SysMenu> {
    /**
     * 通过角色名称查询URL 权限
     *
     * @param role 角色名称
     * @return 菜单列表
     */
    List<SysMenu> findMenuByRoleName(String role);

    /**
     * 根据用户ID获取菜单
     * @param userId
     * @return
     */
    List<SysMenu> findMenuByUserId(Long userId);

    /**
     * 更新排序
     * @param list
     */
    void updateSorts(List<SysMenu> list);

    /**
     * 更新状态
     * @param sysMenu
     */
    void updateStatus(SysMenu sysMenu);
}
