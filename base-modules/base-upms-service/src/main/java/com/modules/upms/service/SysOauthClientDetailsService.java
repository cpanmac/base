package com.modules.upms.service;

import com.common.service.BaseService;
import com.modules.upms.entity.SysOauthClientDetails;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-15
 */
public interface SysOauthClientDetailsService extends BaseService<SysOauthClientDetails> {

}
