package com.modules.upms.service;

import com.common.service.BaseService;
import com.modules.upms.entity.SysRole;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 */
public interface SysRoleService extends BaseService<SysRole> {

    /**
     * 通过部门ID查询角色列表
     * @param deptId 部门ID
     * @return 角色列表
     */
    List<SysRole> selectListByDeptId(Integer deptId);

    /**
     * 角色编码是否唯一
     * @param role
     * @return
     */
    Boolean isRoleCodeExists(SysRole role);

    /**
     * 保存角色菜单权限
     * @param role
     */
    void saveRoleMenuAuth(SysRole role);

    /**
     * 分配用户
     * @param role
     */
    void saveAssignUser(SysRole role);

    /**
     * 删除角色用户的关联
     * @param role
     */
    void deleteRoleUser(SysRole role);
}
