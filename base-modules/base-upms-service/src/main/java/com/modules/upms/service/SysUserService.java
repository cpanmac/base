package com.modules.upms.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.common.service.BaseService;
import com.common.util.R;
import com.modules.upms.entity.SysUser;

import java.util.Date;
import java.util.List;

/**
 */
public interface SysUserService extends BaseService<SysUser> {

    /**
     * 根据用户名查询用户角色信息
     *
     * @param loginName 用户名
     * @return userVo
     */
    SysUser findUserByLoginName(String loginName);

    /**
     * 保存验证码
     *  @param randomStr 随机串
     * @param imageCode 验证码*/
    void saveImageCode(String randomStr, String imageCode);

    /**
     * 通过手机号查询用户信息
     * @param mobile 手机号
     * @return 用户信息
     */
    SysUser findUserByMobile(String mobile);

    /**
     * 通过openId查询用户
     * @param openId openId
     * @return 用户信息
     */
    SysUser findUserByOpenId(String openId);

    /**
     * 修改密码
     * @param id
     * @param password
     * @param lastPasswordResetDate
     */
    void updateUserPasswordById(Long id, String password, Date lastPasswordResetDate);

    /**
     * 根据角色查询用户列表
     * @param sysUser
     * @return
     */
    List<SysUser> getUsersByRoleId(SysUser sysUser);

    /**
     * 登录账号或手机号是否存在
     * @param sysUser
     * @return
     */
    Boolean isExists(SysUser sysUser);

    /**
     * 更新状态
     * @param sysUser
     */
    void updateStatus(SysUser sysUser);
}
