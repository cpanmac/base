package com.modules.upms.service.impl;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.lang.Dict;
import cn.hutool.core.util.StrUtil;
import com.baomidou.dynamic.datasource.spring.boot.autoconfigure.DataSourceProperty;
import com.common.util.CodeAutoGeneratorUtil;
import com.modules.upms.entity.SysMenu;
import com.modules.upms.mapper.CodeGenerateMapper;
import com.modules.upms.mapper.SysMenuMapper;
import com.modules.upms.service.CodeGenerateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 代码生成 服务实现类
 * </p>
 */
@Service
public class CodeGenerateServiceImpl implements CodeGenerateService {

    @Autowired
    private CodeGenerateMapper codeGenerateMapper;

    @Autowired
    private SysMenuMapper sysMenuMapper;

    @Value("${auto-generate.datasource.database}")
    private String database;

    @Value("${auto-generate.datasource.username}")
    private String username;

    @Value("${auto-generate.datasource.password}")
    private String password;

    @Value("${auto-generate.datasource.driver-class-name}")
    private String driverClassName;

    @Value("${auto-generate.datasource.url}")
    private String url;

    @Value("${auto-generate.browse}")
    private String browse;

    @Value("${auto-generate.java}")
    private String java;

    @Override
    public List<Map<String, Object>> getTableList() {
        return codeGenerateMapper.getTableList(database);
    }

    @Override
    public List<Map<String, Object>> getTableColumnList(String tableName) {
        return codeGenerateMapper.getTableColumnList(database, tableName);
    }

    @Override
    public void generate(Dict param) {
        // 生成菜单
        if (StrUtil.isNotBlank(param.getStr("menuName"))) {
            generateMenu(param);
        }

        DataSourceProperty dataSourceProperty = new DataSourceProperty();
        dataSourceProperty.setDriverClassName(driverClassName);
        dataSourceProperty.setUrl(url);
        dataSourceProperty.setUsername(username);
        dataSourceProperty.setPassword(password);

        param.set("browsePath", browse);
        param.set("javaPath", java);
        param.set("isFileOverride", true);
        CodeAutoGeneratorUtil.codeAutoGenerator(param, dataSourceProperty);
    }

    /**
     * 生成菜单
     *
     * @param param
     */
    private void generateMenu(Dict param) {
        // 新建菜单
        String path = StrUtil.format("/{}", param.getStr("moduleName"));
        SysMenu parentMenu = sysMenuMapper.getByPath(path);
        String parentId = "-1";
        if(parentMenu != null) {
            parentId = parentMenu.getId().toString();
        }
        if (parentMenu == null) {
            int sort = sysMenuMapper.getMaxSort(path);
            SysMenu sysMenu = new SysMenu();
            sysMenu.setName(param.getStr("menuName"));
            sysMenu.setLocalesKey(param.getStr("menuLocalesKey"));
            sysMenu.setIcon(param.getStr("menuIcon"));
            sysMenu.setPath(StrUtil.format("/{}", param.getStr("moduleName")));
            sysMenu.setUrl(StrUtil.format("/{}/{}/**", param.getStr("moduleName"), param.getStr("tableJavaName")));
            sysMenu.setStatus("VALID");
            sysMenu.setType("MENU");
            sysMenu.setSort(sort+1);
            sysMenu.setParentId(Convert.toLong(parentId));
            sysMenu.setPermission(StrUtil.format("{}:{}", param.getStr("moduleName"), param.getStr("tableJavaName")));
            sysMenuMapper.insert(sysMenu);

            parentId = sysMenu.getId().toString();
        }
        if (!StrUtil.equals(parentId, "-1")) {
            SysMenu sysMenu = new SysMenu();
            sysMenu.setName(param.getStr("menuName"));
            sysMenu.setLocalesKey(param.getStr("menuLocalesKey"));
            sysMenu.setIcon(param.getStr("menuIcon"));
            sysMenu.setPath(StrUtil.format("/{}/{}", param.getStr("moduleName"), param.getStr("tableJavaName")));
            sysMenu.setUrl(StrUtil.format("/{}/{}/**", param.getStr("moduleName"), param.getStr("tableJavaName")));
            sysMenu.setStatus("VALID");
            sysMenu.setType("MENU");
            sysMenu.setSort(1);
            sysMenu.setParentId(Convert.toLong(parentId));
            sysMenu.setPermission(StrUtil.format("{}:{}", param.getStr("moduleName"), param.getStr("tableJavaName")));
            sysMenuMapper.insert(sysMenu);

            SysMenu sysQueryMenu = new SysMenu();
            sysQueryMenu.setParentId(sysMenu.getId());
            sysQueryMenu.setName(param.getStr("menuName") + "查询");
            sysQueryMenu.setUrl(StrUtil.format("/{}/{}/**", param.getStr("moduleName"), param.getStr("tableJavaName")));
            sysQueryMenu.setStatus("VALID");
            sysQueryMenu.setType("BUTTON");
            sysQueryMenu.setSort(1);
            sysQueryMenu.setPermission(StrUtil.format("{}:{}:query", param.getStr("moduleName"), param.getStr("tableJavaName")));
            sysMenuMapper.insert(sysQueryMenu);

            SysMenu sysAddMenu = new SysMenu();
            sysAddMenu.setParentId(sysMenu.getId());
            sysAddMenu.setName(param.getStr("menuName") + "新增");
            sysAddMenu.setUrl(StrUtil.format("/{}/{}/**", param.getStr("moduleName"), param.getStr("tableJavaName")));
            sysAddMenu.setStatus("VALID");
            sysAddMenu.setType("BUTTON");
            sysAddMenu.setSort(2);
            sysAddMenu.setPermission(StrUtil.format("{}:{}:add", param.getStr("moduleName"), param.getStr("tableJavaName")));
            sysMenuMapper.insert(sysAddMenu);

            SysMenu sysEditMenu = new SysMenu();
            sysEditMenu.setParentId(sysMenu.getId());
            sysEditMenu.setName(param.getStr("menuName") + "修改");
            sysEditMenu.setUrl(StrUtil.format("/{}/{}/**", param.getStr("moduleName"), param.getStr("tableJavaName")));
            sysEditMenu.setStatus("VALID");
            sysEditMenu.setType("BUTTON");
            sysEditMenu.setSort(3);
            sysEditMenu.setPermission(StrUtil.format("{}:{}:edit", param.getStr("moduleName"), param.getStr("tableJavaName")));
            sysMenuMapper.insert(sysEditMenu);

            SysMenu sysDelMenu = new SysMenu();
            sysDelMenu.setParentId(sysMenu.getId());
            sysDelMenu.setName(param.getStr("menuName") + "删除");
            sysDelMenu.setUrl(StrUtil.format("/{}/{}/**", param.getStr("moduleName"), param.getStr("tableJavaName")));
            sysDelMenu.setStatus("VALID");
            sysDelMenu.setType("BUTTON");
            sysDelMenu.setSort(4);
            sysDelMenu.setPermission(StrUtil.format("{}:{}:del", param.getStr("moduleName"), param.getStr("tableJavaName")));
            sysMenuMapper.insert(sysDelMenu);
        }
    }
}
