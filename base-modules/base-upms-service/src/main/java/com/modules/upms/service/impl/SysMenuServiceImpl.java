package com.modules.upms.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.common.service.BaseServiceImpl;
import com.modules.upms.entity.SysMenu;
import com.modules.upms.mapper.SysMenuMapper;
import com.modules.upms.service.SysMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 菜单权限表 服务实现类
 * </p>
 */
@Service
public class SysMenuServiceImpl extends BaseServiceImpl<SysMenuMapper, SysMenu> implements SysMenuService {

    @Autowired
    private SysMenuMapper sysMenuMapper;

    @Override
    public List<SysMenu> findMenuByRoleName(String role) {
        return sysMenuMapper.findMenuByRoleName(role);
    }

    @Override
    public List<SysMenu> findMenuByUserId(Long userId) {
        // 系统管理员，查询所有的菜单
        if(userId == 1L) {
            return sysMenuMapper.getList(null);
        }
        return sysMenuMapper.findMenuByUserId(userId);
    }

    @Override
    public void updateSorts(List<SysMenu> list) {
        if(CollUtil.isNotEmpty(list)) {
            list.forEach(sysMenu -> sysMenuMapper.updateSort(sysMenu));
        }
    }

    @Override
    public void updateStatus(SysMenu sysMenu) {
        sysMenuMapper.updateStatus(sysMenu);
    }
}
