package com.modules.upms.service.impl;

import com.common.service.BaseServiceImpl;
import com.modules.upms.mapper.SysOauthClientDetailsMapper;
import com.modules.upms.entity.SysOauthClientDetails;
import com.modules.upms.service.SysOauthClientDetailsService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-15
 */
@Service
public class SysOauthClientDetailsServiceImpl extends BaseServiceImpl<SysOauthClientDetailsMapper, SysOauthClientDetails> implements SysOauthClientDetailsService {

}
