package com.modules.upms.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.common.service.BaseServiceImpl;
import com.modules.upms.entity.SysRole;
import com.modules.upms.mapper.SysRoleMapper;
import com.modules.upms.service.SysRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 */
@Service
public class SysRoleServiceImpl extends BaseServiceImpl<SysRoleMapper, SysRole> implements SysRoleService {

    @Autowired
    private SysRoleMapper sysRoleMapper;

    /**
     * 通过部门ID查询角色列表
     *
     * @param deptId 部门ID
     * @return 角色列表
     */
    @Override
    public List<SysRole> selectListByDeptId(Integer deptId) {
        return sysRoleMapper.selectListByDeptId(deptId);
    }

    @Override
    public Boolean isRoleCodeExists(SysRole role) {
        return sysRoleMapper.isRoleCodeExists(role) >= 1;
    }

    @Override
    public boolean saveOrUpdate(SysRole role) {
        boolean res = super.saveOrUpdate(role);
        sysRoleMapper.deleteRoleDept(role);
        if(StrUtil.equals(role.getDataScope(), "SCOPE_DETAIL")) {
            if(CollUtil.isNotEmpty(role.getDeptList())) {
                //更新角色数据权限
                sysRoleMapper.insertRoleDept(role);
            }
        }
        return res;
    }

    @Override
    public void saveRoleMenuAuth(SysRole role) {
        sysRoleMapper.deleteRoleMenu(role);
        sysRoleMapper.insertRoleMenu(role);
    }

    @Override
    public void saveAssignUser(SysRole role) {
        sysRoleMapper.saveAssignUser(role);
    }

    @Override
    public void deleteRoleUser(SysRole role) {
        sysRoleMapper.deleteRoleUser(role);
    }
}
