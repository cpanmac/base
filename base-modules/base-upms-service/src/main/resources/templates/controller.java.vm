package ${package.Controller};

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.common.util.R;
import ${package.Entity}.${entity};
import ${package.Service}.${table.serviceName};

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Api;
#if(${restControllerStyle})
import org.springframework.web.bind.annotation.RestController;
#else
import org.springframework.stereotype.Controller;
#end
#if(${superControllerClassPackage})
import ${superControllerClassPackage};
#end

/**
 * <p>
 * $!{table.comment} 前端控制器
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
@Api(tags = {"${entity}" }, description = "${table.comment}相关接口")
#if(${restControllerStyle})
@RestController
#else
@Controller
#end
@RequestMapping(value = "#if(${package.ModuleName})/${package.ModuleName}#end/#if(${controllerMappingHyphenStyle})${controllerMappingHyphen}#else${table.entityPath}#end")
public class ${table.controllerName} extends#if(${superControllerClass}) ${superControllerClass} #end{

    /**
     * ${table.comment}服务类
     */
    @Autowired
    private ${entity}Service ${entity.substring(0, 1).toLowerCase()}${entity.substring(1)}Service;

    /**
     * 返回${table.comment}列表
     *
     * @return ${table.comment}列表
     */
    @ApiOperation(value = "${table.comment}列表", response = ${entity}.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "current", value = "第几页", required = true, dataType = "int", paramType = "query", defaultValue = "1"),
            @ApiImplicitParam(name = "size", value = "每页行数", required = true, dataType = "int", paramType = "query", defaultValue = "20")#if (!$null.isNull($cfg.queryList) || !$cfg.queryList.size()==0),#end

        #foreach($query in ${cfg.queryList})
            @ApiImplicitParam(name = "$query.columnJavaName", value = "$query.columnComment", required = false, dataType = "$query.dataType", paramType = "query")#if($foreach.last)#else,#end

        #end
    })
    @GetMapping(value = "/listPage")
    public R<IPage<${entity}>> listPage(@ModelAttribute Page page, @ModelAttribute ${entity} query){
        return R.success(${entity.substring(0, 1).toLowerCase()}${entity.substring(1)}Service.getPage(page, query));
    }

    /**
     * 保存${table.comment}
     *
     * @param ${entity.substring(0, 1).toLowerCase()}${entity.substring(1)} 实体
     * @return success/false
     */
    @PostMapping(value = "/save")
    @ApiOperation(value = "${table.comment}保存")
    public R<Boolean> save(@RequestBody ${entity} ${entity.substring(0, 1).toLowerCase()}${entity.substring(1)}){
        return R.success(${entity.substring(0, 1).toLowerCase()}${entity.substring(1)}Service.saveOrUpdate(${entity.substring(0, 1).toLowerCase()}${entity.substring(1)}));
    }

    /**
     * 删除${table.comment}
     *
     * @param id ID
     * @return success/false
     */
    @DeleteMapping(value = "/{id}")
    @ApiOperation(value = "根据ID删除${table.comment}")
    public R<Boolean> delete(@PathVariable Long id){
        return R.success(${entity.substring(0, 1).toLowerCase()}${entity.substring(1)}Service.deleteById(id));
    }

}
