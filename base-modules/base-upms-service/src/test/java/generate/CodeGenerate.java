package generate;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.lang.Dict;
import com.baomidou.dynamic.datasource.spring.boot.autoconfigure.DataSourceProperty;
import com.common.util.CodeAutoGeneratorUtil;
import com.modules.WebApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {WebApplication.class})
public class CodeGenerate {

    @Test
    public void generate() {
        String browsePath = "D:\\workspace\\base\\base-modules\\base-upms-service\\";
        String javaPath = "D:\\workspace\\base\\base-modules\\base-upms-service\\";
        DataSourceProperty dataSourceProperty = new DataSourceProperty();
        dataSourceProperty.setDriverClassName("com.mysql.cj.jdbc.Driver");
        dataSourceProperty.setUrl("jdbc:mysql://127.0.0.1:3306/base?useSSL=false&useUnicode=true&characterEncoding=utf8&autoReconnect=true&serverTimezone=Hongkong");
        dataSourceProperty.setUsername("root");
        dataSourceProperty.setPassword("root");

        Dict baseInfo = new Dict();
        baseInfo.set("tableField", "sys_user");
        baseInfo.set("moduleName", "sys");
        baseInfo.set("author", "熊猛辉");
        baseInfo.set("browsePath", browsePath);
        baseInfo.set("javaPath", javaPath);
        baseInfo.set("isFileOverride", true);

        Dict query1 = new Dict();
        query1.set("tableField", "user_name");
        query1.set("field", "userName");
        query1.set("label", "用户名");
        query1.set("type", "String");
        Dict query2 = new Dict();
        query1.set("tableField", "login_name");
        query2.set("field", "loginName");
        query2.set("label", "登录名");
        query2.set("type", "String");
        Dict query3 = new Dict();
        query1.set("tableField", "mobile");
        query3.set("field", "mobile");
        query3.set("label", "手机");
        query3.set("type", "String");

        Dict conf = new Dict();
        conf.put("baseInfo", baseInfo);
        conf.put("queryList", CollUtil.newArrayList(query1, query2, query3));
        conf.put("listInfo", CollUtil.newArrayList(query1, query2, query3));
        conf.put("formInfo", CollUtil.newArrayList(query1, query2, query3));
        CodeAutoGeneratorUtil.codeAutoGenerator(conf, dataSourceProperty);
    }
}
